
from microWebSrv import MicroWebSrv
import network
import time
import pycom

###########################################################

wifi_ssid = 'gnet'
wifi_pass = 'kalorifer'

###########################################################

content = """\
    <!DOCTYPE html>
    <html>
    <head>
    <link rel='stylesheet' type='text/css' href='style.css'>
    </head>
    <body>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <h2>LED Color Picker</h2>

        <label for="color-input" id="color-label" style="background-color: red"></label>
        <input type="checkbox" id="color-input" checked></input>

        <div id="color-picker">
            <canvas id="color-block" height="250" width="250"></canvas>
            <canvas id="color-strip" height="250" width="20"></canvas>
        </div>
        <script src="script.js" type="text/javascript"></script>
    </body>
    </html>
    """

@MicroWebSrv.route('/')
def _httpHandlerLed(httpClient, httpResponse):
    a = httpClient.ReadRequestContent()
    print(a)
    httpResponse.WriteResponseOk(headers = None,
                                    contentType = "text/html",
                                    contentCharset = "UTF-8",
                                    content = content )

@MicroWebSrv.route('/led/<index>') # <IP>/led/123 -> args['index']=123
def _httpHandlerLedApi(httpClient, httpResponse, args={}):

    pycom.rgbled(args['index'])

    httpResponse.WriteResponseOk(headers = None,
                                    contentType = "text/html",
                                    contentCharset = "UTF-8",
                                    content = None )

def _acceptWebSocketCallback(webSocket, httpClient):
    print("ws accept")
    webSocket.RecvTextCallback   = _recvTextCallback
    webSocket.RecvBinaryCallback = _recvBinaryCallback
    webSocket.ClosedCallback     = _closedCallback

def _recvTextCallback(webSocket, msg):
    #print("ws text: %s" % msg)
    color = msg.split(',')

    r = (int(color[0]) * 0x7f) // 255
    g = (int(color[1]) * 0x7f) // 255
    b = (int(color[2]) * 0x7f) // 255

    v = '{:02x}{:02x}{:02x}'.format(r, g, b)
    pycom.rgbled(int(v, 16))

def _recvBinaryCallback(webSocket, data):
    #print("ws bin: %s" % data)
    pass

def _closedCallback(webSocket) :
    print("ws closed")
    pycom.rgbled(0)

pycom.heartbeat(False)

wlan = network.WLAN(mode=network.WLAN.STA)
wlan.connect(wifi_ssid, auth=(network.WLAN.WPA2, wifi_pass))
while not wlan.isconnected():
    time.sleep_ms(50)

print(wlan.ifconfig())

ws = MicroWebSrv()
ws.MaxWebSocketRecvLen = 256
ws.WebSocketThreaded = False
ws.AcceptWebSocketCallback = _acceptWebSocketCallback
ws.Start(threaded=True)
